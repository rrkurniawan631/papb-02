package com.example.login;

import android.os.Bundle;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class ResultActivity extends AppCompatActivity {
    TextView textResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textResult = findViewById(R.id.textViewResult);

        String username = getIntent().getStringExtra("username");
        String password = getIntent().getStringExtra("password");

        String resultText = "Username: " + username + "\nPassword: " + password;
        textResult.setText(resultText);
    }
}